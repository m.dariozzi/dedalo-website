export const environment = {
  apiUrl: 'https://api.ilbancodirita.it/dedalo-website-api',
  assetUrl: 'https://api.ilbancodirita.it/dedalo-website-api/v0/image/getImageByName/',
  production: true,
};
