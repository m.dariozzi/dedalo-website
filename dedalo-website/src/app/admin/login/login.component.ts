import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { JwtService } from 'src/app/_services/jwt.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/_services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	submitted = false;
	loading = false;
	returnUrl: string;

	// email = new FormControl('c@c.c', [Validators.required]);
	// password = new FormControl('Azerty123', [Validators.required]);

	email = new FormControl('', [Validators.required]);
	password = new FormControl('', [Validators.required]);

	constructor(private jwt: JwtService, private router: Router, private alertService: AlertService) { }

	ngOnInit(): void { }

	loginForm = new FormGroup({
		email: this.email,
		password: this.password
	}); 

	get f() { return this.loginForm.controls; }

	onSubmit(){
		this.submitted = true;

		if (this.loginForm.invalid) {
			return;
		}

		this.loading = true;
		this.jwt.login(this.f.email.value, this.f.password.value).subscribe(
		data => {
			if (data) {
				this.jwt.requestToken(this.f.email.value, this.f.password.value).subscribe(data => {
					localStorage.setItem('access_token', JSON.stringify(data));
				})

				this.router.navigate(['dashboard']);
			} else {
				this.alertService.error('Je email of wachtwoord zijn niet geldig');
				this.loading = false;
			}
		}, 
		error => {
			this.alertService.error('Je email of wachtwoord zijn niet geldig');
			this.loading = false;
		},);
	}

}
