import { Component, OnInit } from '@angular/core';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { environment } from 'src/environments/environment';
import { AlertService } from 'src/app/_services/alert.service';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-appointment-management',
  templateUrl: './appointment-management.component.html',
  styleUrls: ['./appointment-management.component.css']
})
export class AppointmentManagementComponent implements OnInit {
	env = environment;
	events = [];
	public isCollapsed = true;
	submitted = false;
	stalls;

	edit_mode = false;

	


	id = new FormControl();
	title = new FormControl('', [Validators.required]);
	from_date = new FormControl(null, [Validators.required]);
	to_date = new FormControl(null, [Validators.required]);
	start_time = new FormControl('', [Validators.required]);
	stop_time = new FormControl('', [Validators.required]);
	assigned_stall = new FormControl('', [Validators.required]);

	constructor(private data: DataAccessService, private alert: AlertService) { }
	

	ngOnInit(): void {
		this.retrieveData();
	}

	createForm = new FormGroup({
		id: this.id,
		title: this.title,
		from_date: this.from_date,
		to_date: this.to_date,
		start_time: this.start_time,
		stop_time: this.stop_time,
		assigned_stall: this.assigned_stall,	
	});

	get f() { return this.createForm.controls; }

	checkStatus(start_time, stop_time) {
		var current_date = new Date().getTime();
		let start = new Date("1970-01-01 " + start_time).getTime();
		let end = new Date("1970-01-01 " + stop_time).getTime();

		if (end < current_date) {
			return "over";
		} else if ((current_date <= end && current_date >= start)) {
			return "in progress";
		} else if (end > current_date) {
			return "upcoming";
		} 
	}


	retrieveData() {
		this.events = [];
		
		this.data.getAllAppointments().subscribe(data => {
			data.forEach(element => {
				this.events.push({ 
					title: element.title,
					name: element.name,
					textColor: 'white',
					color: 'green',
					start_date: element.start_date,
					end_date: element.end_date,

					start_time: element.start_time,
					stop_time: element.stop_time,

					id: element.id,
					city: element.city,
					description: element.description,
					street_square: element.street_square,
					appointment: element.appointment,
					path: element.path,
					extension: element.extension,
				   	stall_id: element.stalls_id});
			});
		});

		this.data.getAllStalls().subscribe(data => {
			this.stalls = data;
		});

	}

	addAppointment() {
		this.edit_mode = false;	
		this.createForm.reset();
		this.toggleForm();
	}


	editAppointment(event) {
		this.edit_mode = true;
		
		if (this.isCollapsed) {
			this.isCollapsed = !this.isCollapsed;
		}

		var split_start_time = event.start_time.split(':');
		var split_stop_time = event.stop_time.split(':');

		var parsed_start_date = this.parseDMY(event.start_date);
		var parsed_end_date = this.parseDMY(event.end_date);
		var parsed_start_time = { hour: parseInt(split_start_time[0]), minute: parseInt(split_start_time[1])};
		var parsed_stop_time = { hour: parseInt(split_stop_time[0]), minute: parseInt(split_stop_time[1])}; 
		
	
		this.id.setValue(event.id);
		this.title.setValue(event.title);
		this.assigned_stall.setValue(event.stall_id);
		this.from_date.setValue(new NgbDate(parsed_start_date.getUTCFullYear(),  parsed_start_date.getUTCMonth() + 1, parsed_start_date.getDate()));
		this.to_date.setValue(new NgbDate(parsed_end_date.getUTCFullYear(),  parsed_end_date.getUTCMonth() + 1, parsed_end_date.getDate()));
		this.start_time.setValue(parsed_start_time);
		this.stop_time.setValue(parsed_stop_time);
	}	
	
	onSubmit() {
		this.submitted = true;

		if (this.createForm.invalid) {	
			return;
		}	
	
		const data = {
			title: this.f.title.value,
			assigned_stall: this.f.assigned_stall.value,
			from_date: new Date(this.from_date.value.year, this.from_date.value.month - 1, this.from_date.value.day).toLocaleString(),
			to_date: new Date(this.to_date.value.year, this.to_date.value.month - 1, this.to_date.value.day).toLocaleString(),
			start_time: this.start_time.value,
			stop_time: this.stop_time.value
		}

		
		this.data.addAppointment(data).subscribe(data => {
			this.submitted = false;
			this.alert.succes('New appointment added');
			this.retrieveData();
			this.toggleForm();
		},
		error => {
			this.submitted = false;
			this.alert.error('error adding new appointment' + error);
			return;
		});

	}

	toggleForm() {
		this.isCollapsed = !this.isCollapsed;
	}

	onSubmitEdit() {
		this.submitted = true;

		console.log(this.createForm);

		if (this.createForm.invalid) {
			console.log('form invalid');
			return;
		}
		
		const data = {
			id: this.f.id.value,
			title: this.f.title.value,
			assigned_stall: this.f.assigned_stall.value,
			from_date: new Date(this.from_date.value.year, this.from_date.value.month - 1, this.from_date.value.day).toLocaleString(),
			to_date: new Date(this.to_date.value.year, this.to_date.value.month - 1, this.to_date.value.day).toLocaleString(),
			start_time: this.start_time.value,
			stop_time: this.stop_time.value
		}
		
		this.data.editAppointment(data).subscribe(data => {
			this.submitted = false;
			this.alert.succes('Appointment changed successfully');
			this.retrieveData();
			this.toggleForm();
		},
		error => {
			this.submitted = false;
			this.alert.error('error editing this appointment' + error);
			return;
		});
	}

	deleteAppointment(event) {
		if (confirm(`Are you sure you want to remove ${event.title} ?`)) {
			this.data.deleteAppointment(event).subscribe(resp => {
				this.alert.succes('Appointment deleted successfully');
				//refresh data
				this.retrieveData();
			},
			error => {
				console.log(error.error);
				this.alert.error('error deleteing appointment' + error.error);
			});
		}
			
	}

	parseDMY(s) {
		var b = s.split(/\D+/);
		var d = new Date(b[2], b[1]-1, b[0]);
		d.setFullYear(b[2]);
		return d && d.getMonth() == b[1]-1? d : new Date(NaN);
	}
}
