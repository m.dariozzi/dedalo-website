import { Component, OnInit } from '@angular/core';
import { JwtService } from 'src/app/_services/jwt.service';
import { Router } from '@angular/router';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	active = 1;
	constructor(private jwt: JwtService, private router: Router) { }

	ngOnInit(): void {
		if (localStorage.getItem('dashboard_active_page') !== null) {
			this.active = Number(localStorage.getItem('dashboard_active_page'));
		}
	}
	
	simpleGETRequest() {
		this.jwt.simpleGETRequest().subscribe(data => {
			console.log(data);
		}, 
		error => {
			console.log(error);
		});
	}

	AuthGETRequest() {
		this.jwt.AuthGETRequest().subscribe(data => {
			console.log(data);
		}, 
		error => {
			console.log(error);
		});
	}

	simplePOSTRequest() {
		this.jwt.simplePOSTRequest().subscribe(data => {
			console.log(data);
		}, 
		error => {
			console.log(error);
		});
	}

	AuthPOSTRequest() {
		this.jwt.AuthPOSTRequest().subscribe(data => {
			console.log(data);
		}, 
		error => {
			console.log(error);
		});
	}

	requestToken() {
		this.jwt.requestToken('c@c.c', 'Azerty123').subscribe(data => {
			console.log(data);	
		}, error => {
			console.log(error);
		});
	}

	onNavChange(changeEvent: NgbNavChangeEvent) {	
		localStorage.setItem('dashboard_active_page', changeEvent.nextId);
	}




}
