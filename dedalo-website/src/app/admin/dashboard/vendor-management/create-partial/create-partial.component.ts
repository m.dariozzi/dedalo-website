import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { AlertService } from 'src/app/_services/alert.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Vendor } from 'src/app/vendor';

@Component({
  selector: 'app-create-partial',
  templateUrl: './create-partial.component.html',
  styleUrls: ['./create-partial.component.css']
})
export class CreatePartialComponent implements OnInit {
	@Input() vendor: Vendor;

	submitted = false;
	loading = false;

	constructor(private data: DataAccessService, private alert: AlertService,
		 public activeModal: NgbActiveModal) { }

	firstname = new FormControl('', [Validators.required]);
	lastname = new FormControl('', [Validators.required]);

	ngOnInit(): void {
		//edit mode
		if (this.vendor) {
			this.firstname.setValue(this.vendor.firstname);
			this.lastname.setValue(this.vendor.name);
		}	
	}

	createForm = new FormGroup({
			firstname: this.firstname,
			lastname: this.lastname
	}); 

	get f() { return this.createForm.controls; }

	onSubmit() {
		this.submitted = true;

		if (this.createForm.invalid) {
			return;
		}
		
		this.loading = true;

		this.data.insertNewVendor(this.f.lastname.value, this.f.firstname.value).subscribe(data => {
			if (data) {
					console.log(data);
					this.alert.succes('vendor aangemaakt');
					this.activeModal.close("success");
			} else {
					this.alert.error('Er is iets misgegaan');
					this.loading = false;
			}
		}, 
		error => {
			console.log(error);
			this.alert.error('Er is iets misgegaan ' + error.message);
			this.loading = false;
		});
	}

	onSubmitEdit() {
		this.submitted = true;

		if (this.createForm.invalid) {
			return;
		}

		this.loading = true;

		this.data.editVendor(this.vendor.id, this.f.lastname.value, this.f.firstname.value).subscribe(data => {
			if (data) {
				console.log(data);
				this.alert.succes(`Edited vendor ${this.vendor.id}`);
				this.activeModal.close("success");
			} else {
				this.alert.error('something went wrong');
			}
		},
		error => {
			console.log(error);
			this.loading = false;

			this.alert.error('er is iets misgegaan ' + error.message);
			this.activeModal.close('error');
		});
	}
}
