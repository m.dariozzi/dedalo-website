import { Component, OnInit } from '@angular/core';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { Stall } from 'src/app/stall';
import { AlertService } from 'src/app/_services/alert.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CreatePartialComponent } from './create-partial/create-partial.component';
import { ModalpartialComponent } from '../modalpartial/modalpartial.component';

@Component({
  selector: 'app-vendor-management',
  templateUrl: './vendor-management.component.html',
  styleUrls: ['./vendor-management.component.css']
})
export class VendorManagementComponent implements OnInit {

	vendors;
	stalls;
	vendorStalls = new Map<Number, Stall[]>();

	constructor(private data: DataAccessService, private alert: AlertService, private modalService: NgbModal) { }

	ngOnInit(): void {
		this.data.getAllVendors().subscribe(data => {
			this.vendors = data;
		});
		

		this.data.getAllStalls().subscribe(data => {
			this.stalls = data;
		});
	}

	toggleCollapseForVendorStalls(element: HTMLElement, vendorId) {
		element.classList.toggle('d-none');
		this.data.getStallsForVendor(vendorId).subscribe(data => {
			this.vendorStalls.set(vendorId, data);
		});
	}

	/**
	 * event handlers for CRUD Operations
	 */


	//ADD
	addVendor() {
		const modalRef = this.modalService.open(CreatePartialComponent).result.then((result) => {
			this.data.getAllVendors().subscribe(data => { this.vendors = data; })
		},
		reason => {
			if (reason === ModalDismissReasons.ESC) {
				console.log('closed by escape')
				this.alert.warning('Adding vendor aborted');
			}
		});
	}


	//EDIT
	editVendor(vendor) {
		const modalRef = this.modalService.open(CreatePartialComponent);
		modalRef.componentInstance.vendor = vendor
		modalRef.result.then((result) => {
			this.data.getAllVendors().subscribe(data => { this.vendors = data; });
		},
		reason => {
			this.alert.warning('Editing vendor aborted');
		});
	}

	// DELETE SINGLE VENDOR
	deleteVendor(vendor) {
		if (confirm(`Are you sure to remove this ${vendor.firstname} ${vendor.name} ?`)) {
			this.data.deleteVendor(vendor.id).subscribe(resp => {
				this.alert.succes(`Vendor ${vendor.firstname} ${vendor.name} has been deleted successfully`);
				this.data.getAllVendors().subscribe(data => this.vendors = data);
			},
			error => {
				console.log(error.error);
				this.alert.error(`error deleting this vendor ${error}`);
			});
		}
	}


	//DELETE ASSOCIATED STALL FROM VENDOR
	deleteStallFromVendor(vendor, stall) {
		if (confirm(`Are you sure to remove stall ${stall.name} ${stall.city} from vendor ${vendor.firstname} ${vendor.name} ?`)) {
			
			this.data.deleteStallFromVendor(vendor.id, stall.id).subscribe(resp => {
				this.alert.succes(`Stall ${stall.name} deleted successfully from vendor ${vendor.firstname} ${vendor.name}`);
				this.data.getStallsForVendor(vendor.id).subscribe(data => { this.vendorStalls.set(vendor.id, data)});
			},
			error => {
				this.alert.error(`error deleting this stall ${error}`);
			});
		}
	}

	openNewStallPopup(vendor) {
		const modalRef = this.modalService.open(ModalpartialComponent);
		modalRef.componentInstance.vendor = vendor;
		modalRef.result.then((result) => {
			this.data.getStallsForVendor(vendor.id).subscribe(data => {this.vendorStalls.set(vendor.id, data)});
		},
		reason => {
			if (reason === ModalDismissReasons.ESC) {
				console.log('closed by escape')
				this.alert.warning('Adding stall to vendor aborted');
			}
		});
	}

}
