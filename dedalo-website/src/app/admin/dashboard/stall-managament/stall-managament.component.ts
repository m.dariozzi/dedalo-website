	import { Component, OnInit } from '@angular/core';
	import { DataAccessService } from 'src/app/_services/data-access.service';
import { AlertService } from 'src/app/_services/alert.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CreatePartialComponent as stallCreatePartialComponent } from './create-partial/create-partial.component';


@Component({
selector: 'app-stall-managament',
templateUrl: './stall-managament.component.html',
styleUrls: ['./stall-managament.component.css']
})
export class StallManagamentComponent implements OnInit {

	stalls
	constructor(private data: DataAccessService, private alert: AlertService, private modalService: NgbModal) { }

	ngOnInit(): void {
		this.data.getAllStalls().subscribe(data => {
			this.stalls = data;
		});
		
	}


	//ADD
	addStall() {
		const modalRef = this.modalService.open(stallCreatePartialComponent).result.then((result) => {
			this.data.getAllStalls().subscribe(data => { this.stalls = data; })
		},
		reason => {
			if (reason === ModalDismissReasons.ESC) {
				this.alert.warning('Adding stall aborted');
			}
		});
	}


	// EDIT
	editStall(stall) {
		const modalRef = this.modalService.open(stallCreatePartialComponent);
		modalRef.componentInstance.stall = stall
		modalRef.result.then((result) => {
			this.data.getAllStalls().subscribe(data => { this.stalls = data; });
		},
		reason => {
			this.alert.warning('Editing stall aborted');
		});
	}

	// DELETE SINGLE VENDOR
	deleteStall(stall) {
		if (confirm(`Are you sure to remove this ${stall.name} ?`)) {
			this.data.deleteStall(stall.id).subscribe(resp => {
				this.alert.succes(`Stall ${stall.name} has been deleted successfully`);
				this.data.getAllStalls().subscribe(data => this.stalls = data);
				console.log(resp);
			},
			error => {
				console.log(error.error);
				this.alert.error(`error deleting this stall ${error.message}`);
			});
		}
	}

}
