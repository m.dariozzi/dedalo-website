	import { Component, OnInit, Input } from '@angular/core';
	import { Stall } from 'src/app/stall';
	import { DataAccessService } from 'src/app/_services/data-access.service';
	import { AlertService } from 'src/app/_services/alert.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-create-partial',
	templateUrl: './create-partial.component.html',
	styleUrls: ['./create-partial.component.css']
})
export class CreatePartialComponent implements OnInit {
@Input() stall: Stall;

submitted = false;
loading = false;

constructor(private data: DataAccessService, private alert: AlertService,
	public activeModal: NgbActiveModal) { }

	name = new FormControl('', [Validators.required]);
	city = new FormControl('', [Validators.required]);
	street = new FormControl('', [Validators.required]);
	appointment = new FormControl('', [Validators.required]);
	description = new FormControl('', [Validators.required]);

	ngOnInit(): void {
		//edit mode
		
		if(this.stall) {
			this.name.setValue(this.stall.name);
			this.city.setValue(this.stall.city);
			this.street.setValue(this.stall.street_square);

			this.appointment.setValue(this.stall.appointment);
			this.description.setValue(this.stall.description);
		}
	}

	createForm = new FormGroup({
		name: this.name,
		city: this.city,
		street: this.street,
		appointment: this.appointment,
		description: this.description
	});

	get f() { return this.createForm.controls; }

	onSubmit() {
		this.submitted = true;

		if (this.createForm.invalid) {
			return;
		}

		this.loading = true;

		this.data.insertNewStall(this.f.name.value, this.f.city.value, this.f.street.value, this.f.appointment.value, this.f.description.value).subscribe(data => {
			if (data) {
				this.alert.succes('stall aangemaakt');
				this.activeModal.close('success');
			} else {
				this.alert.error('Er is iets misgegaan');
				this.loading = false;
			}
		},
		error => {
			this.alert.error('Er is iets misgegaan ' + error.message);
			this.loading = false;
		});
	}

	onSubmitEdit() {
		this.submitted = true;

		if (this.createForm.invalid) {
			return;
		}


		this.loading = true;

		this.data.editStall(this.stall.id, this.f.name.value, this.f.city.value, this.f.street.value, this.f.appointment.value, this.f.description.value).subscribe(data => {
			if (data) {
				this.alert.succes(`Edited Stall ${this.stall.id}`);
				this.activeModal.close('success');
			} else {
				this.alert.error('something went wrong');
			}
		},
		error => {
			this.loading = false;

			this.alert.error('er is iets misgegaan ' + error.message);
			this.activeModal.close('error');

		});
	}
}
