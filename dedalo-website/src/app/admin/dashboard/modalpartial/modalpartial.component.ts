import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { computeEventEndResizable } from '@fullcalendar/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { element } from 'protractor';
import { elementAt } from 'rxjs/operators';
import { Vendor } from 'src/app/vendor';
import { AlertService } from 'src/app/_services/alert.service';
import { DataAccessService } from 'src/app/_services/data-access.service';

@Component({
	selector: 'app-modalpartial',
	templateUrl: './modalpartial.component.html',
	styleUrls: ['./modalpartial.component.css']
})
export class ModalpartialComponent implements OnInit {
	@Input() vendor: Vendor;

	submitted = false;
	loading = false;

	form: FormGroup;

	allStalls = [];
	vendorStalls = [];

	constructor(private data: DataAccessService, private alert: AlertService, public activeModal: NgbActiveModal, private formBuilder: FormBuilder) {
		this.form = this.formBuilder.group({
			stalls: new FormArray([])
		});

	}

	ngOnInit(): void {
	
		if (this.vendor) {
			this.data.getAllStalls().subscribe(result1 => {

				this.allStalls = result1;
				this.data.getStallsForVendor(this.vendor.id).subscribe(result2 => {
					this.vendorStalls = result2;	
					
					this.addCheckboxes();
	
				},
				error => {
					console.log(error);
				});
				
			},
			error => {
				console.log(error);
			});
		}
	}

	
	get stallsFromArray() {
		return this.form.controls.stalls as FormArray;
	}

	private addCheckboxes() {
		const toRemove = [];
		this.allStalls.forEach(sta => {
			const isPresent = this.vendorStalls.filter(x => x.id === sta.id);
			if (isPresent.length > 0) {
				toRemove.push(sta);				
			} else {
				this.stallsFromArray.push(new FormControl(false)); 
			}
		});
		
		toRemove.forEach(t => {
			this.allStalls.splice(this.allStalls.indexOf(t), 1);
		});
	}

	
	get f() { return this.form.controls; }

	onSubmit() {
		this.submitted = true;

		if (this.form.invalid) {
			return;
		}

		this.loading = true;

		const selectedStalls = this.form.value.stalls
			.map((checked, i) => checked ? this.allStalls[i] : null)
			.filter(v => v !== null);

		
		this.data.addStallsToVendor(selectedStalls, this.vendor.id).subscribe(result => {
			if (result) {
				console.log(result);
				this.alert.succes(`Added mercati per ${this.vendor.name}`);
				this.activeModal.close("success");
			} else {
				this.alert.error('something went wrong');
			}
		},
		error => {
			console.log(error);
			this.loading = false;

			this.alert.error('er is iets misgegaan ' + error.message);
			this.activeModal.close('error');
		});
	}

}