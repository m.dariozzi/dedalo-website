import { Component, OnInit, Input } from '@angular/core';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { AlertService } from 'src/app/_services/alert.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { Image } from 'src/app/image';
import { Stall } from 'src/app/stall';

@Component({
  selector: 'app-edit-partial',
  templateUrl: './edit-partial.component.html',
  styleUrls: ['./edit-partial.component.css']
})
export class EditPartialComponent implements OnInit {
	@Input() image;
	@Input() stalls: Stall[];

	env = environment;

	submitted = false;
	loading = false;

	hovervalue;
	form: FormGroup;
	
	constructor(private data: DataAccessService, private alert: AlertService,
		public activeModal: NgbActiveModal) { }

	ngOnInit(): void { 

		this.form = new FormGroup({
			'dropdown': new FormControl('all', [])
		})
	}
	
	get s() { return this.form.get('dropdown').value; }
	get f() { return this.form.controls; }

	onSubmit() {
		this.submitted = true;

		if (this.form.invalid) {
			return;
		}

		this.loading = true;

		//edit stall for the image
		this.data.editImageToNewStall(this.image.id, this.s).subscribe(
		resp => {
			this.activeModal.close();
		},
		err => {
			this.activeModal.dismiss(err);
		})
	}

	onStallOptionsSelected(value) {
		this.hovervalue = this.stalls.filter(x => x.id == value)[0];
	}
}
