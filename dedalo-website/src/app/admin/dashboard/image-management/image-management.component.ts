import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { EditPartialComponent } from './edit-partial/edit-partial.component';
import { AlertService } from 'src/app/_services/alert.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-image-management',
  templateUrl: './image-management.component.html',
  styleUrls: ['./image-management.component.css']
})
export class ImageManagementComponent implements OnInit {
	env = environment;
	/** 
	 * @TODO
	 * 5) add CRUD operations for existing images for stalls
	 * 6) refresh data wanneer er acties gedaan worden
	 * 7) error message verbeteren
	 * 8) silent login invoeren
	 * 9) routing op de server fixen want die shit is kapot, F5 op welke pagina dan ook exit de front-end rounting
	 */

	stalls;
	stallImages;
	editImageForm: FormGroup;

	public EditImagesIsCollapsed = false;


	constructor(private formBuilder: FormBuilder, private data: DataAccessService,
		private modalService: NgbModal, private alert: AlertService) {
			 
		this.editImageForm = new FormGroup({
				stallDropdown: new FormControl(null, Validators.required)
		});
		this.editImageForm.controls['stallDropdown'].setValue('stall', { onlySelf: true });
	}

	ngOnInit(): void {
		this.data.getAllStalls().subscribe(data => {
			this.stalls = data;
		});

	}

	onStallOptionsSelected(event) {
		let stallId = event.split(':')[1].trim();

		this.data.getImagesForStall(stallId).subscribe(data => {
			this.stallImages = data;
		});
	}


	editImage(img) {
		const modalRef = this.modalService.open(EditPartialComponent);
		modalRef.componentInstance.image = img;
		modalRef.componentInstance.stalls = this.stalls;
		modalRef.result.then((result) => {
			this.alert.succes('Image edited successfully');
			this.getdata();
		},
		reason => {
			if (reason === ModalDismissReasons.ESC) {
				this.alert.warning('changing stall aborted');
			} else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
				this.alert.warning('changing stall aborted');
			}else {			
				if (reason) {
					this.alert.error(reason.message);
				}
			}
		});
	}
						
	deleteImage(img) {
		if (confirm(`Are you sure to remove this image from ${img.path} from this stall ${img.name} ?`)) {
			
			this.data.deleteImage(img).subscribe(resp => {
				this.alert.succes(`Image ${img.path} deleted successfully`);
				this.getdata();
			},
			error => {
				this.alert.error(`error deleting this image ${error}`);
			});
		}
	}

	getdata() {
		this.data.getAllStalls().subscribe(data => this.stalls = data);
	}

}
