import { Component, OnInit, Input } from '@angular/core';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, Form } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertService } from 'src/app/_services/alert.service';
import { FileUploadService } from 'src/app/_services/file-upload.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Stall } from 'src/app/stall';



@Component({
	selector: 'app-image-creation',
	templateUrl: './image-creation.component.html',
	styleUrls: ['./image-creation.component.css']
})
export class ImageCreationComponent implements OnInit {
	@Input() stalls : Stall[]; 

	/**
	 * @TODO
	 * 1) fix css on form inside table, padding and margin are not ok
	 * 5) add CRUD operations for existing images for stalls
	 */


	submittedNewImages = false;
	loading = false;

	masterForm: FormGroup;

	IsCollapsed = true;

	filesToUpload = [];
	imgPaths = [];


	constructor(private sanitizer: DomSanitizer,
		private formBuilder: FormBuilder, private alert: AlertService,
		private fileUpload: FileUploadService) {

		this.masterForm = this.formBuilder.group({});

	}

	ngOnInit(): void {
	}

	get f() { return this.masterForm.controls; }

	getNestedControl(index, controlname) { return this.f.childForms.get(index + "." + controlname); }

	onSubmitNewImages() {
		this.masterForm.markAllAsTouched();

		this.submittedNewImages = true;


		if (this.masterForm.invalid) {
			this.alert.error('please select a stall to link to the image');
			return;
		}

		this.loading = true;

		if (this.filesToUpload.length == 0) {
			this.alert.error('no files selected');
		}


		this.filesToUpload.forEach(element => {
			this.fileUpload.uploadFile(element, this.masterForm.value.childForms[this.filesToUpload.indexOf(element)]).subscribe(
				event => {
					if (event.type == HttpEventType.UploadProgress) {
						const percentDone = Math.round(100 * event.loaded / event.total);
						console.log(`File is ${percentDone}% loaded`);
					} else if (event instanceof HttpResponse) {
						console.log('file is completely loaded');
						console.log(event);
					}
				},
				(err) => {
					console.log('error', err.error);
					this.alert.error('error' + err.error);

					this.loading = false;
					this.submittedNewImages = false;
					return;
				},
				() => {
					this.loading = false;
					this.alert.succes('file upload succeeded');

					//reset the masterform
					for (let i = this.filesToUpload.length; i >= 0; i--) {
						this.removeImageFromList(i);
					}
				},
			)
		});
	}


	/**
	 * Deletes formgroup form masterForm for given index
	 * deletes thumbnail image from array for given index
	 * delets file from FileArray for given index
	 * @param index index/name of the formgroup that is to be deleted
	 */
	removeImageFromList(index) {
		let array = this.masterForm.controls['childForms'] as FormArray;
		array.removeAt(index);
		this.filesToUpload.splice(index, 1)
		this.imgPaths.splice(index, 1);
	}

	handleFileInput(files: FileList) {
		Array.prototype.push.apply(this.filesToUpload, files);
		this.imgPaths = [];

		this.createPreviewThumbnailsforFilesToUpload(this.filesToUpload);

		this.masterForm = this.formBuilder.group({
			childForms: this.formBuilder.array(
				Array.from(this.filesToUpload).map(x => this.formBuilder.group({
					stallForImage: ['', [Validators.required]],
				})))
		});
	}


	/**
	 * Helper function, reads uploaded images as Base64
	 * */

	createPreviewThumbnailsforFilesToUpload(files) {
		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {

			var reader = new FileReader();

			// Read in the image file as a data URL.

			reader.onload = ev => {
				this.imgPaths.push(ev.target.result);
			};
			reader.readAsDataURL(f);
		}
	}
}
