import { Component, OnInit } from '@angular/core';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { AlertService } from 'src/app/_services/alert.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { CreatePartialComponent as ProductCreatePartialComponent } from './create-partial/create-partial.component';

@Component({
selector: 'app-gallery-management',
templateUrl: './gallery-management.component.html',
styleUrls: ['./gallery-management.component.css']
})
export class GalleryManagementComponent implements OnInit {
	env = environment;
	products;
	constructor(private data: DataAccessService, private alert: AlertService, private modalService: NgbModal) { }

	ngOnInit(): void {
		this.data.getAllProducts().subscribe(data => {
			this.products = data;		
		});
	}


	addProduct() {
		const modalRef = this.modalService.open(ProductCreatePartialComponent).result.then((result) => {
			this.data.getAllProducts().subscribe(data => { this.products = data; })
		},
		reason => {
			if (reason == ModalDismissReasons.ESC) {
				this.alert.warning('adding product failed');
			}
		});
	}

	editProduct(product) {
		const modalRef = this.modalService.open(ProductCreatePartialComponent);
		modalRef.componentInstance.product = product;
		modalRef.result.then((result) => {
			this.data.getAllProducts().subscribe(data => { this.products = data; })
		},
		reason => {
			this.alert.warning('Editing product aborted');
		});
	}	


	//DELETE product
	deleteProduct(product) {
		if (confirm(`Are you sure you want to remove ${product.name} ?`)) {
			this.data.deleteProduct(product).subscribe(resp => {
				this.alert.succes('Product deleted successfully');
				this.data.getAllProducts().subscribe(data => { this.products = data; })
			},
			error => {
				console.log(error.error);
				this.alert.error('error deleting product' + error.error);
			});
		}
		
	}

}
