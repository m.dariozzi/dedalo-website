import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/product';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataAccessService } from 'src/app/_services/data-access.service';
import { AlertService } from 'src/app/_services/alert.service';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { FileUploadService } from 'src/app/_services/file-upload.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-create-partial',
	templateUrl: './create-partial.component.html',
	styleUrls: ['./create-partial.component.css']
})
export class CreatePartialComponent implements OnInit {
	@Input() product;

	submitted = false;
	loading = false;
	filesToUpload = [];
	env = environment;

	thumbnailPic = [];

	constructor(private data: DataAccessService, private alert: AlertService,
		public activeModal: NgbActiveModal, private fileUploadService: FileUploadService) { }

	name = new FormControl('', [Validators.required]);
	code = new FormControl('', [Validators.required]);
	description = new FormControl('', [Validators.required]);
	price = new FormControl('', [Validators.required]);
	file = new FormControl('', []);

	ngOnInit(): void {

		if (this.product) {
			this.name.setValue(this.product.name);
			this.code.setValue(this.product.code);
			this.description.setValue(this.product.description);
			this.price.setValue(this.product.price);
		}
	}

	createForm = new FormGroup({
		name: this.name,
		code: this.code,
		description: this.description,
		price: this.price,
		file: this.file
	});

	get f() { return this.createForm.controls; }

	onSubmit() {
		this.submitted = true;

		if (this.createForm.invalid) {
			return;
		}

		this.loading = true;
		const p = {
			name: this.f.name.value,
			code: this.f.code.value,
			description: this.f.description.value,
			price: this.f.price.value
		};


		this.data.addProduct(p).subscribe(data => {

			if (data) {
				this.filesToUpload.forEach(element => {
					this.fileUploadService.uploadFileForProduct(element, 'product', data.product_id).subscribe(
						event => {
							if (event.type == HttpEventType.UploadProgress) {
								const percentDone = Math.round(100 * event.loaded / event.total);
								console.log(`File is ${percentDone}% loaded`);
							} else if (event instanceof HttpResponse) {
								console.log('file is completely loaded');
								console.log(event);
							}
						},
						(err) => {

							this.alert.error('error' + err.error);

							this.loading = false;
							this.submitted = false;
							return;
						},
						() => {
							this.loading = false;
							this.alert.succes('file upload succeeded');
							this.activeModal.close('success');
						}
					);
				});
				
			} else {
					this.loading = false;
			}
		
			},
			error => {
				this.alert.error('something went wrong' + error.message);
				this.loading = false;
			});
	}




onSubmitEdit() {
	this.submitted = true;
	if (this.createForm.invalid) {
		return;
	}

	this.loading = true;

	const p = {
		id: this.product.id,
		name: this.f.name.value,
		code: this.f.code.value,
		description: this.f.description.value,
		price: this.f.price.value,
		old_path: this.product.path,
		old_extension: this.product.extension
	}
	console.log(p);

	this.data.editProduct(JSON.stringify(p), this.product.image_id, this.filesToUpload[0]).subscribe(
		resp => {
			console.log(resp);
			this.activeModal.close();
		},
		err => {
			this.activeModal.dismiss(err);
		})
}

handleFileInput(files: FileList) {
	Array.prototype.push.apply(this.filesToUpload, files);

	for (var i = 0, f; f = this.filesToUpload[i]; i++) {
		var reader = new FileReader();
		reader.onload = ev => {
			this.thumbnailPic.push(ev.target.result);
		};
		reader.readAsDataURL(f);
		console.log(this.thumbnailPic);
	}
}

}
