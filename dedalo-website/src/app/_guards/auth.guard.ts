import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { JwtService } from '../_services/jwt.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	constructor(private router: Router, private jwt: JwtService) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

		const userToken = this.jwt.loggedIn;
		const currentUser = this.jwt.currentUserValue;

		if (userToken && currentUser) {
			return true;
		} else {
			this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
			return false;
		} 
		
	}	
}
	
