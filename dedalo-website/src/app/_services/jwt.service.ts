import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../user';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class JwtService {

	public currentUser: Observable<User>;
	private currentUserSubject: BehaviorSubject<User>;

	private url = environment.apiUrl;

	constructor(private http: HttpClient) {
		this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('current_user')));
		this.currentUser = this.currentUserSubject.asObservable();
	}

	public get currentUserValue(): User {
		return this.currentUserSubject.value;
	}

	login(email: string, password: string) {
		return this.http.post<{access_token: string, user: User}>(`${this.url}/auth/login`, {email, password})
		.pipe(tap(res => { 
			localStorage.setItem('current_user', JSON.stringify(res.user));
			this.currentUserSubject.next(res.user);
		}));
		
	} 

	register(email: string, password: string) {
		// return this.http.post<{access_token: string}>('http://localhost:8080/auth/register', {email, password}).pipe(tap(res => {
		return this.http.post<{access_token: string}>(`${this.url}/auth/register`, {email, password}).pipe(tap(res => {
			this.login(email, password);
		}))
	}

	logout() {
		localStorage.removeItem('current_user');
		localStorage.removeItem('access_token');

		this.currentUserSubject.next(null);
	}

	requestToken(email: string, password: string) {
		return this.http.post<{access_token: string}>(`${this.url}/api/tokens`, {email, password});
	}


	public get loggedIn(): boolean{
		return localStorage.getItem('current_user') !==  null;
	}

	public get istokenPresent(): boolean {
		return localStorage.getItem('access_token') !== null;
	}

	public getAccessToken() {
		return JSON.parse(localStorage.getItem('access_token'));
	}


	public simpleGETRequest() {
		return this.http.get(`${this.url}/test/simpleGETRequest`);
	}
	
	public AuthGETRequest() {
		return this.http.get(`${this.url}/test/AuthGETRequest`);
	}
	
	public simplePOSTRequest() {
		return this.http.post<{result: string}>(`${this.url}/test/simplePOSTRequest`, {test: 'test'});
	}

	public AuthPOSTRequest() {
		return this.http.post<{result: string}>(`${this.url}/test/AuthPOSTRequest`, {test: 'test'});
	}
}
