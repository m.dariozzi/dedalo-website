import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

	private url = environment.apiUrl + '/v0';

	constructor(private http: HttpClient) { }

	uploadFile(file: File, data): Observable<HttpEvent<any>> {
		
		let formData = new FormData();
		let params = new HttpParams();

		formData.append('photo', file);
		formData.append('stallForImage', data.stallForImage);

		const options = {
			params: params,
			reportProgress: true,
		};
		
		const req = new HttpRequest('post', `${this.url}/images/save`, formData, options);
		return this.http.request(req).pipe(map(resp => resp as any));
	}


	uploadFiles(files: any[], data) {
		files.forEach(element => {
			this.uploadFile(element, data[files.indexOf(element)]).pipe(map(resp => resp as any));
		});
	}

	uploadFileForProduct(file: File, target, target_id): Observable<HttpEvent<any>> {
		let formData = new FormData();
		let params = new HttpParams();

		formData.append('photo', file);
		formData.append('target', target);
		formData.append('target_id', target_id);

		const options = {
			params: params,
			reportProgress: true
		};

		const req = new HttpRequest('post', `${this.url}/products/image/add`, formData, options);
		return this.http.request(req).pipe(map(resp => resp as any));
		}
}
