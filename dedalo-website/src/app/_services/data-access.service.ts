import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Stall } from '../stall';
import { Vendor } from '../vendor';
import { Image } from '../image';
import { map } from 'rxjs/operators';
import { Product } from '../product';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class DataAccessService {	
	

	private url = environment.apiUrl + '/v0';

	constructor(private http: HttpClient) { }

	/**
	 * Stalls API
 	*/

	// stalls/
	public getAllStalls() {
		return this.http.get<Stall[]>(`${this.url}/stalls`).pipe(map(resp => resp as Stall[]));
	}

	// stalls/:id
	public getStallById(id) {
		let params = new HttpParams().set('id', id);
		return this.http.get(`${this.url}/stalls/${id}`, /*{ params: id }*/).pipe(map(resp => resp as Stall));
	}                    
		
	//vendors/:id/stalls
	public getStallsForVendor(id) {
		return this.http.get<Stall[]>(`${this.url}/vendors/${id}/stalls`).pipe(map(resp => resp as Stall[]));
									
	}


	// stalls/:id/images
	public getImagesForStall(stall_id) {
		return this.http.get<Image[]>(`${this.url}/stalls/${stall_id}/images`).pipe(map(resp => resp as Image[]));
	}

	// images/stalls
	public getStallImages() {
		return this.http.get<Image[]>(`${this.url}/images/stalls`).pipe(map(resp => resp as Image[]));
	}

	//images/name/stalls
	public getImagesForStallByName(stall_name) {
		return this.http.get<Image[]>(`${this.url}/images/${stall_name}/images`).pipe(map(resp => resp as Image[]));
	}

	public getStallByName(stall_name) {
		return this.http.get<Stall>(`${this.url}/stalls/${stall_name}`).pipe(map(resp => resp as Stall));
	}

	// /stalls/:id/images/random
	public getRandomImageForStallById(stall_id) {
		return this.http.get<Image>(`${this.url}/stalls/${stall_id}/images/random`).pipe(map(resp => resp as Image));
	}

	/**
	 * INSERTS
	 */

	public insertNewStall(name: string, city: string, street_square: string, appointment: string, description: string) {
		return this.http.post<{stallId: any}>(`${this.url}/stalls/add`, {name, city, street_square, appointment, description}).pipe(map(resp => resp as any));
	}

	 /**
	 * EDITS
	 */

	public editStall(id: number, name: string, city: string, street_square: string, appointment: string, description: string) {
		return this.http.post<{affected_rows: any}>(`${this.url}/stalls/edit`, {id, name, city, street_square, appointment, description}).pipe(map(resp => resp as any));
	}

	public editImageToNewStall(image_id: number, stall_id: number) {
		return this.http.post<{affected_rows: any}>(`${this.url}/images/stall/edit`, {image_id, stall_id}).pipe(map(resp => resp as any));
	}


	/**
	 * DELETEs
	 */

	public deleteImage(img: Image) {
		return this.http.post<{affected_rows: any}>(`${this.url}/images/delete`, {img}).pipe(map(resp => resp as any));
	}	



	/**
	 * Vendor API
	 */

	public getAllVendors() {
		return this.http.get<Vendor>(`${this.url}/vendors`).pipe(map(resp => resp as Vendor));
	}

	public getVendorById(id) {
		return this.http.get<Vendor>(`${this.url}/vendors/${id}`).pipe(map(resp => resp as Vendor));
	}

	/**
	 * INSERT ACTIONS
	 */
	public insertNewVendor(lastname: string, firstname: string) {
		return this.http.post<{vendorId: any}>(`${this.url}/vendors/add`, {lastname, firstname}).pipe(map(resp => resp as any));
	}

	public addStallsToVendor(selectedStalls, vendor_id) {
		return this.http.post<{affected_rows: any}>(`${this.url}/vendors/stalls/add`, {selectedStalls, vendor_id}).pipe(map(resp => resp as any));
	}

	/**
	 * EDIT ACTIONS
	 */
	public editVendor(id: number, lastname: string, firstname: string) {
		return this.http.post<{affected_rows: any}>(`${this.url}/vendors/edit`, {id, lastname, firstname}).pipe(map(resp => resp as any));
	}

	/**
	 * DELETE ACTIONS
	 */
	public deleteVendor(vendor_id: number) {
		return this.http.post<{vendor_id: number}>(`${this.url}/vendor/delete`, {vendor_id}).pipe(map(resp => resp as any));
	}

	public deleteStallFromVendor(vendor_id: number, stall_id: number) {
		return this.http.post<{vendor_id: number, stall_id: number}>(`${this.url}/vendors/stall/delete`, {vendor_id, stall_id}).pipe(map(resp => resp as any));
	}

	public deleteStall(stall_id: number) {
		return this.http.post<{stall_id: number}>(`${this.url}/stall/delete`, {stall_id}).pipe(map(resp => resp as any));
	}



	/**
	 * Products API
	 */
	public getAllProducts() {
		return this.http.get<Product>(`${this.url}/products`).pipe(map(resp => resp as Product));
	}

	public getProductById(id) {
		return this.http.get<Product>(`${this.url}/products/${id}`).pipe(map(resp => resp as Product));
	}

	public getImagesForProduct(id) {
		return this.http.get(`${this.url}/products/${id}/images`).pipe(map(resp => resp as any));
	}

	public addProduct(product) {
		return this.http.post<{productId: any}>(`${this.url}/products/add`, {product}).pipe(map(resp => resp as any));
	}


	/*
		EDIT
	*/
	public editProduct(product, image_id, photo: File) {
		let formdata = new FormData();
		formdata.append('photo', photo);
		formdata.append('product', product);
		formdata.append('image_id', image_id);
		

		const req = new HttpRequest('post', `${this.url}/products/edit`, formdata);
		// const req = new HttpRequest('post', `${this.url}/products/edit`, {product, formdata, image_id});
		return this.http.request(req).pipe(map(resp => resp as any));
		// return this.http.post<{affected_rows:any}>(`${this.url}/products/edit`, {product}).pipe(map(resp => resp as any));
	}

	public deleteProduct(product) {
		return this.http.post<{product_id: number}>(`${this.url}/products/delete`, {product}).pipe(map(resp => resp as any));
	}


	/**
	* Date API
	*/

	public getAppointmentsFromWeb() {
		return this.http.get<any>(`${this.url}/appointments/1`);
	}

	public getAllAppointments() {
		return this.http.get<any>(`${this.url}/appointments`);
	}

	public getAppointmentsByStallId(id) {
		return this.http.get<any>(`${this.url}/appointments/stalls/${id}`);
	}

	public addAppointment(data) {
		return this.http.post<{productId: any}>(`${this.url}/appointments/add`, {data}).pipe(map(resp => resp as any));
	}

	public editAppointment(data) {
		return this.http.post<{appointment_id}>(`${this.url}/appointments/edit`, {data}).pipe(map(resp => resp as any));
	}

	public deleteAppointment(event) {
		return this.http.post<{appointment_id: number}>(`${this.url}/appointments/delete`, {event}).pipe(map(resp => resp as any));
	}



	/**
	 * Test routes
	 */
	public testToken() {
		return this.http.get<any>(`${this.url}/testtoken`);
	}


}	
