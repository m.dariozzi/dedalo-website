import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WebsiteHeaderComponent } from './_layout/website/website-header/website-header.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { WebsiteFooterComponent } from './_layout/website/website-footer/website-footer.component';
import { GalleryComponent } from './gallery/gallery.component';
import { DetailComponent } from './detail/detail.component';
import { NgbModule, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { StallComponent } from './stall/stall.component';
import { CalendarComponent } from './calendar/calendar.component';
import { LoaderComponent } from './shared/loader/loader.component';
import { LoaderService } from './_services/loader.service';
import { LoaderInterceptor } from './_interceptors/loader.interceptor';
import { NgxGalleryModule } from  '@kolkov/ngx-gallery';
import { PageNotFoundComponent } from './_exceptions/page-not-found/page-not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FullCalendarModule } from '@fullcalendar/angular';
import { LoginComponent } from './admin/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AlertComponent } from './shared/alert/alert.component';
import { CalendarPartialComponent } from './calendar/calendar-partial/calendar-partial.component';
import { CardListPartialComponent } from './calendar/card-list-partial/card-list-partial.component';
import { WebsiteLayoutComponent } from './_layout/website/website-layout/website-layout.component';
import { AppHeaderComponent } from './_layout/app/app-header/app-header.component';
import { AppLayoutComponent } from './_layout/app/app-layout/app-layout.component';
import { VendorManagementComponent } from './admin/dashboard/vendor-management/vendor-management.component';
import { CreatePartialComponent as VendorCreatePartialComponent } from './admin/dashboard/vendor-management/create-partial/create-partial.component';
import { CreatePartialComponent as StallCreatePartialComponent } from './admin/dashboard/stall-managament/create-partial/create-partial.component';
import { CreatePartialComponent as ProductCreatePartialComponent} from './admin/dashboard/gallery-management/create-partial/create-partial.component';
import { HttpBearerTokenInterceptor } from './_interceptors/http-bearer-token.interceptor';
import { StallManagamentComponent } from './admin/dashboard/stall-managament/stall-managament.component';
import { ImageManagementComponent } from './admin/dashboard/image-management/image-management.component';
import { ImageCreationComponent } from './admin/dashboard/image-management/image-creation/image-creation.component';
import { EditPartialComponent } from './admin/dashboard/image-management/edit-partial/edit-partial.component';
import { GalleryManagementComponent } from './admin/dashboard/gallery-management/gallery-management.component';
import { CreatePartialComponent } from './admin/dashboard/gallery-management/create-partial/create-partial.component';
import { AppointmentManagementComponent } from './admin/dashboard/appointment-management/appointment-management.component';
import { SafePipe } from './safe.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LightboxModule } from 'ngx-lightbox';
import { ModalpartialComponent } from './admin/dashboard/modalpartial/modalpartial.component';


@NgModule({
  declarations: [
    AppComponent,
    WebsiteHeaderComponent,
    HomeComponent,
    ContactComponent,
    WebsiteFooterComponent,
    GalleryComponent,
    DetailComponent,
    StallComponent,
    CalendarComponent,
    LoaderComponent,
    PageNotFoundComponent,
    LoginComponent,
    DashboardComponent,
    AlertComponent,
    CalendarPartialComponent,
    CardListPartialComponent,
    WebsiteLayoutComponent,
    AppHeaderComponent,
    AppLayoutComponent,
    VendorManagementComponent,
    VendorCreatePartialComponent,
    StallCreatePartialComponent,
    StallManagamentComponent,
    ImageManagementComponent,
    ImageCreationComponent,
    EditPartialComponent,
    GalleryManagementComponent,
    CreatePartialComponent,
    AppointmentManagementComponent,
    SafePipe,
    ModalpartialComponent    
  ],
  entryComponents: [VendorCreatePartialComponent, StallCreatePartialComponent, EditPartialComponent, ProductCreatePartialComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    NgxGalleryModule,
    ReactiveFormsModule,
    FullCalendarModule,
    BrowserAnimationsModule,
    LightboxModule
  ],
  providers: [
    LoaderService, { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
    HttpBearerTokenInterceptor, { provide: HTTP_INTERCEPTORS, useClass: HttpBearerTokenInterceptor, multi: true}, 
    // {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
