import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataAccessService } from '../_services/data-access.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  env = environment;
  images = [];
  rand_nums = [];
  constructor(private data: DataAccessService) {
    this.data.getStallImages().subscribe(data => {

      while(this.rand_nums.length < 3) {
        const number = this.getRandomInt(0, data.length);
          if(!this.rand_nums.includes(number)) {
            this.rand_nums.push(number);
          }
      }

      this.rand_nums.forEach(number => {
        this.images.push(`${data[number].path}${data[number].extension}`)
      });

    });
   }

  ngOnInit(): void { }

  /**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
// https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range

   getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
