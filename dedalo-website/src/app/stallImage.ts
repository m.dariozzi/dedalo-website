import { SafeResourceUrl } from "@angular/platform-browser";

export class StallImage {
    id: number;
    path: string;
    extension: string;
    stalls_id: number;
}
