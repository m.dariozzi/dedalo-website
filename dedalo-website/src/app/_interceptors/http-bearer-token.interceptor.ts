import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { JwtService } from '../_services/jwt.service';
import { Observable, from } from 'rxjs';
import { catchError, flatMap, switchMap } from 'rxjs/operators';

@Injectable()
export class HttpBearerTokenInterceptor implements HttpInterceptor {

	constructor(private jwt: JwtService) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return from(this.handleAccess(request, next));
	}

	private async handleAccess(request: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
	
	let token;
	const currentUser = this.jwt.currentUserValue;

	//voorlopige opslag
	if (this.jwt.istokenPresent) {
		token = this.jwt.getAccessToken().access_token;
	} else if (currentUser && this.jwt.loggedIn) {
		return next.handle(request).toPromise();		
		
	} else {
		return next.handle(request).toPromise();
	}

	let changedRequest = request;
	const headerSettings: {[name: string]: string | string[]; } = {};

	for (const key of request.headers.keys()) {
		headerSettings[key] = request.headers.getAll(key);
	}

	if (token) {
		headerSettings['Authorization'] = 'Bearer ' + token;
	} 

	const newHeader = new HttpHeaders(headerSettings);
	changedRequest = request.clone({headers: newHeader});
	
	return next.handle(changedRequest).toPromise();
	}
}
