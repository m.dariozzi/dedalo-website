import { SafeResourceUrl } from "@angular/platform-browser";

export class Stall {
    id: number;
    name: string;
    city: string;
    street_square: string;
    appointment: string;
    description: string;
}