import { SafeResourceUrl } from "@angular/platform-browser";

export class Image {
    id: number;
    path: string;
    extension: string;
}
