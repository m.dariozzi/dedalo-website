import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent} from './contact/contact.component';
import { GalleryComponent } from './gallery/gallery.component';
import { StallComponent } from './stall/stall.component';
import { CalendarComponent } from './calendar/calendar.component';
import { DetailComponent } from './detail/detail.component';
import { PageNotFoundComponent } from './_exceptions/page-not-found/page-not-found.component';
import { LoginComponent } from './admin/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AuthGuard } from './_guards/auth.guard';
import { WebsiteLayoutComponent } from './_layout/website/website-layout/website-layout.component';
import { AppLayoutComponent } from './_layout/app/app-layout/app-layout.component';


const routes: Routes = [
  {
    path: '', 
    component: WebsiteLayoutComponent,
    children: [
      { path: '', redirectTo: '/index', pathMatch: 'full'},
      { path: 'index', component: HomeComponent },
      { path: 'gallery', component: GalleryComponent},
      { path: 'product/:id', component: DetailComponent,},
      { path: 'contact', component: ContactComponent },
      { path: 'stall', component: StallComponent},
      { path: 'calendar', component: CalendarComponent},    
      { path: 'login', component: LoginComponent},
    ]
  },
  {
    path: '',
    component: AppLayoutComponent,
    children: [      
      { path: 'dashboard', component:DashboardComponent, canActivate: [AuthGuard]},    
    ]
  },

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
