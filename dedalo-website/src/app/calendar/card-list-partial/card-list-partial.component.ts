import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-card-list-partial',
  templateUrl: './card-list-partial.component.html',
  styleUrls: ['./card-list-partial.component.css'],
  inputs: ['calendar-events']
})
export class CardListPartialComponent implements OnInit, OnChanges{
	@Input('calendar-events') events: Array<any>;
	
	env = environment;

	data = [];
	_events = [];

	dropDownList: Array<any>;	

	public isCollapsed = false;
	sortButtonValue: number;

	placeholderImage = '../../../assets/images/placeholder/Placeholder.png';

	constructor() { }

	ngOnInit(): void {	
		this.data = this.events;
		console.log(this.data);
	}

	ngOnChanges() {
		this._events = this.events;
		this.data = this.events;
		if (this.data) { this.sortUp(); }
	}

	sortUp() {
		this.data = this.sortArrayByStartDate(this.data, 'up');
		this.sortButtonValue = 1;
	}

	sortDown() {
		this.data = this.sortArrayByStartDate(this.data, 'down');
		this.sortButtonValue = 2;
	}

	filterOnCurrentDay() {
		this.data = this.filterArrayByDate(this.data);
	}


	/**
	 * @param filterString String to filter on the name
	 */
	filterOnStallName(filterString) {
		this.data = this._events.filter(function (el) {
			return el.title.toLowerCase().indexOf(filterString.toLowerCase()) !== -1
		});
	}

	filterArrayByDate(events) {
		return events.filter(item => {
			let date = new Date(item.start);
			return date >= new Date();
		});
	}


	sortArrayByStartDate(events, direction) {
		return events.sort(function(a, b) {
			if (direction === 'down') {
				return new Date(b.end).getTime() - new Date(a.start).getTime();
			} else if (direction === 'up') {
				return new Date(a.end).getTime() - new Date(b.start).getTime();
			}
			
		});
	}
}
