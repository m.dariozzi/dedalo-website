import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { OptionsInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FullCalendarComponent } from '@fullcalendar/angular';
import itLocale from '@fullcalendar/core/locales/it';
import { DataAccessService } from 'src/app/_services/data-access.service';

@Component({
  selector: 'app-calendar-partial',
  templateUrl: './calendar-partial.component.html',
  styleUrls: ['./calendar-partial.component.css']
})
export class CalendarPartialComponent implements OnInit {
	calendarOptions: OptionsInput;
	@ViewChild('calendar') calendarComponent: FullCalendarComponent;
	@Input('calendar-events') events: String;

	calendarPlugins = [dayGridPlugin];
	locale;
	stallData;

	constructor(private data: DataAccessService) {
		this.locale = itLocale;
	}

	ngOnInit(): void { }
	
	handleEventClick(args) {
		this.data.getStallByName(args.event.title).subscribe(data => {
			this.stallData = data[0];
		});
	}
}
