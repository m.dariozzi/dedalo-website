import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataAccessService } from '../_services/data-access.service';
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-calendar',
	templateUrl: './calendar.component.html',
	styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

months = ['gennaio', 'febbraio', 'marzo', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre'];
dateString = [];

active = 1;
events = [];


export_events;

constructor(private data: DataAccessService) { }

	ngOnInit(): void {

		if (localStorage.getItem('calendar_display_style') !== null) {
			this.active = Number(localStorage.getItem('calendar_display_style'));
		}

		// this.data.getAppointmentsFromWeb().subscribe(data => {
		// 	data = this.stripHtml(data);
		// 	data = data.replace(/\s/g, ' ');
		// 	data = data.split('        ');
		// 	data = data.filter(item => item);

		// 	for (let i = 0; i < data.length; i++) {
		// 		if (data[i].indexOf('PICENO') > -1) {
		// 			this.dateString = data[i+1].substring(7, data[i+1].length).replace(/(202[0-9])/g, '$1:').split(':');
		// 		}
		// 	}

		// 	this.dateString.splice(- 1, 1);
		// 	for (let i = 0; i < this.dateString.length; i++) {
		// 		var str = this.dateString[i];

		// 		// Isolate year from the string
		// 		var rest = str.substring(0, str.lastIndexOf(' ') + 1).trim();
		// 		var year = str.substring(str.lastIndexOf(' ') + 1, str.length).trim();

		// 		// isolate month and day from the rest
		// 		var month = str.substring(rest.lastIndexOf(' '), rest.length).trim();
		// 		rest = rest.substring(0, rest.lastIndexOf(' ') + 1).trim();

		// 		// isolate days from the string
		// 		let days = rest.split(' ');
		// 		// this.addEvent(year, month, days[0], days[days.length - 1], 'test', 'Piazza Arringo', 'piazza Arringo');
		// 	}	 
		// });		


		this.data.getAllAppointments().subscribe(data => {
			console.log(data);
			data.forEach(element => {
			
				this.events.push({ title: element.name, textColor: 'white', color: 'green',
				start: element.start_date, end: element.end_date, description: element.description,
				start_time: element.start_time, stop_time: element.stop_time,
				city: element.city, street_square: element.street_square, appointment: element.appointment,
				path: element.path, extension: element.extension});
			});
			
			
			this.export_events = this.events;
			console.log("export : " + this.export_events);
		});
	

	}

	
	addEvent(year, month, start, finish, description, city, street_square) {
		var numeric_month = this.zeroFill(this.months.indexOf(month.toLowerCase()) + 1, 2);
		this.events.push({ title: 'Ascoli Piceno',	textColor: 'white', color: '#51bcda',
		start: `${year}-${numeric_month}-${parseInt(start)} 08:00:00`, 
		end: `${year}-${numeric_month}-${parseInt(finish) + 1} 18:00:00`,
		description: description, city: city, street_square: street_square}
		); 
	} 

	onNavChange(changeEvent: NgbNavChangeEvent) {	
		localStorage.setItem('calendar_display_style', changeEvent.nextId);
	}

	/**
	 * taken from https://ourcodeworld.com/articles/read/376/how-to-strip-html-from-a-string-extract-only-text-content-in-javascript
	 * Returns the text from a HTML string
	 * 
	 * @param {html} String The html string
	 */
	stripHtml(html){
		// Create a new div element
		var temporalDivElement = document.createElement("div");
		// Set the HTML content with the providen
		temporalDivElement.innerHTML = html;
		// Retrieve the text property of the element (cross-browser support)
		return temporalDivElement.textContent || temporalDivElement.innerText || "";
	}


	zeroFill(number, width) {
		width -= number.toString().length;
		if ( width > 0 ) {
			return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
		}
		return number + ""; // always return a string
	}

	
}

