import { SafeResourceUrl } from "@angular/platform-browser";

export class Product {
    id: number;
    name: string;
    code: string;
    description: string;
    extension: string;
    path: string;
}