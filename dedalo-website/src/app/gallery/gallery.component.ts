import { Component, OnInit } from '@angular/core';
import { DataAccessService } from '../_services/data-access.service';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
	
	env = environment;
	products;
	all_items;


	product_name
	product_description

	
	page = 1;
	pageSize = 10;
	collectionSize;

	constructor(private data: DataAccessService) { }


	ngOnInit(): void {
		this.data.getAllProducts().subscribe(data => {
			this.products = data;
			this.assignCopy();
		});
	}

	assignCopy(){
		this.all_items = Object.assign([], this.products);
		this.collectionSize = this.all_items.length;
	}

	filterItemsOnName(filterString) {
		for (let i = this.products.length -1; i >= 0; --i) {
			if (this.products[i].name.toLowerCase().indexOf(filterString.toLowerCase()) !== -1) {
				this.all_items[i] = this.products[i];
			} else {
				this.all_items.splice(this.products[i], 1);
			}
			
		}
	}

	filterItemsOnDescription(filterString) {
		for (let i = this.products.length - 1; i >= 0; --i) {
			if (this.products[i].description.toLowerCase().indexOf(filterString.toLowerCase()) !== -1) {
				this.all_items[i] = this.products[i];
			} else {
				this.all_items.splice(this.products[i], 1);
			}
		}
	}
}
