import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { DataAccessService } from '../_services/data-access.service';
import { environment } from 'src/environments/environment';
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT } from 'ngx-lightbox';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {


	constructor(private route: ActivatedRoute, private router: Router, private data: DataAccessService,private _lightbox: Lightbox,
		private _lightboxEvent: LightboxEvent, private _lightboxConfig: LightboxConfig)
 	{ }

	env = environment;
	product;
	images: Array<{src: string, caption: string, thumb: string}> = [];
	private _subscription: Subscription;
	


	ngOnInit(): void {


		this.product = this.route.paramMap.pipe(switchMap((params: ParamMap) =>
			this.data.getProductById(params.get('id')))).subscribe(data => {
				this.product = data[0];

				this.data.getImagesForProduct(this.product.id).subscribe(res => {

					res.forEach(element => {
						this.images.push({
							src: this.env.assetUrl + element.path + element.extension,
							caption: element.description,
							thumb: this.env.assetUrl + element.path + element.extension
						});
					});
				});
			});
	}

	gotoGallery() {
		this.router.navigate(['/gallery']);
	}

	open(index: number): void {
		// open lightbox
		this._lightbox.open(this.images, index, {wrapAround: true, centerVertically: true});
		this._subscription = this._lightboxEvent.lightboxEvent$
		.subscribe(event => this._onReceivedEvent(event));
	  }
	
	  close(): void {
		// close lightbox programmatically
		this._lightbox.close();
	  }

	  private _onReceivedEvent(event: any): void {
		// remember to unsubscribe the event when lightbox is closed
		if (event.id === LIGHTBOX_EVENT.CLOSE) {
		  // event CLOSED is fired
		  this._subscription.unsubscribe();
		}
	
		if (event.id === LIGHTBOX_EVENT.OPEN) {
		  // event OPEN is fired
		  console.log('is open');
		}
	
		if (event.id === LIGHTBOX_EVENT.CHANGE_PAGE) {
		  // event change page is fired
		  console.log(event.data); // -> image index that lightbox is switched to
		}
	  }

}
