import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataAccessService } from '../_services/data-access.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from '@kolkov/ngx-gallery';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-stall',
  templateUrl: './stall.component.html',
  styleUrls: ['./stall.component.css']
})
export class StallComponent implements OnInit {
galleryOptions: NgxGalleryOptions[];
galleryImages: NgxGalleryImage[];


env = environment;
images = [];

selectedIndex: number = null;

section;
subsection;

stalls;
stall

vendors;  
vendor;

constructor(private data: DataAccessService) { }

ngOnInit(): void {
	this.galleryOptions = [
		{
			width: '70%',
			height: '500px',
			thumbnailsColumns: 4,
			thumbnailsRemainingCount: true,
			imageAnimation: NgxGalleryAnimation.Slide,
			previewCloseOnClick: true,
			previewCloseOnEsc: true,
		},
		// max-width 800
		{
			breakpoint: 800,
			width: '100%',
			height: '600px',
			imagePercent: 80,
			thumbnailsPercent: 20,
			thumbnailsMargin: 20,
			thumbnailMargin: 20
		},
		// max-width 400
		{
			breakpoint: 400,
			preview: false
		}

	]; 


	this.data.getAllVendors().subscribe(data => {
		this.vendors = data;
		this.toggleSection(this.vendors[0]);
	});
}

	toggleSection(vendor) {
		this.section = true;
		this.vendor = vendor;
		this.galleryImages = [];

		
		
		this.data.getStallsForVendor(vendor.id).subscribe(data => {
			this.stalls = data;
		});

	}

	toggleSubSection(stall, i) {
		this.subsection = true;
		this.galleryImages = [];

		this.data.getImagesForStall(stall.id).subscribe(data => {
			data.forEach(element => {
			
				this.galleryImages.push(
					{ small: `${this.env.assetUrl}${element.path}${element.extension}`,
					medium: `${this.env.assetUrl}${element.path}${element.extension}`,
					big: `${this.env.assetUrl}${element.path}${element.extension}` });
			});
		});

		this.selectedIndex = i;
		this.stall = stall;
	}
}
