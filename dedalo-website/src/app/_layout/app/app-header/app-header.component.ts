import { Component, OnInit } from '@angular/core';
import { JwtService } from 'src/app/_services/jwt.service';
import { Router } from '@angular/router';
import { User } from 'src/app/user';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {

  user;
  constructor(private jwt: JwtService, private router: Router) { }

  ngOnInit(): void {
    this.user = this.jwt.currentUserValue;
  }

  logout() {
		this.jwt.logout();
		this.router.navigate(['login']);
	}

}
