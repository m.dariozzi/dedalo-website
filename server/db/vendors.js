require('dotenv').config();
const mysql = require('mysql');

const conn = new mysql.createConnection({
    host: 'localhost',
    user: 'matteo',
    password: 'mysql',
    database: 'dedalo_website',
    port: 3306,
    debug: true
});

const getVendors = (req, res) => {
    conn.query(`SELECT * FROM vendors`, (error, results) => {
        if (error) { res.status(500).json({ message: error.message}); }
        res.status(200).json(results);
    });

}

const getVendorsById = (req, res) => {
    const id = req.params.id;
    conn.query('SELECT * FROM vendors WHERE id = ?', [id], (error, results) => {
        if (error) { res.status(500).json( { message: error.message }); }
        res.status(200).json(results);
    });
}

const getStallsForVendorsByVendorId = (req, res) => {    
    const id = req.params.id;
    conn.query(`SELECT stalls.id, stalls.name, stalls.city, stalls.street_square, stalls.appointment, stalls.description FROM vendors 
    INNER JOIN vendors_has_stalls ON vendors.id = vendors_has_stalls.vendors_id
    INNER JOIN stalls ON stalls.id = vendors_has_stalls.stalls_id
    WHERE vendors.id = ?
    GROUP BY stalls.id;`, [id], (error, results) => {
        if (error) { res.status(500).json({ message: error.message }); }
        res.status(200).json(results)
    }); 

}

module.exports = {
    getVendors,
    getVendorsById,
    getStallsForVendorsByVendorId
};