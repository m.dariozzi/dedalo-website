const mysql = require('mysql');

const conn = new mysql.createConnection({
    host: 'localhost',
    user: 'matteo',
    password: 'mysql',
    database: 'dedalo_website',
    port: 3306,
    debug: true
});


const getStalls = (req, res) => {
    conn.query(`select * from stalls`, (error, results) => {
        if (error) { res.status(500).json({ message: error.message}); }
        res.status(200).json(results)
    });
}

const getStallsById = (req, res) => {
    const id = req.params.id;
    conn.query(`select * from stalls where stalls.id = ?`, [id], (error, results) => {
        if (error) { res.status(500).json({ message: error.message}); }
        res.status(200).json(results);
    });
}

const getImagesForStall = (req, res) => {
    const id = req.params.id;
    conn.query(`SELECT * FROM stalls_images 
    INNER JOIN stalls ON stalls_id = stalls.id 
    WHERE stalls.id = ?;`, [id], (error, results) => {
        if (error) { res.status(500).json({ message: error.message }) }
        res.status(200).json(results);
    });
}

const getImagesForStallByName = (req, res) => {
    const name = req.params.name;
    conn.query(`SELECT * from stalls_images INNER JOIN stalls ON stalls_id = stalls.id
                WHERE stalls.name LIKE ? OR CITY LIKE ?`, [name, name], (error, results) => {
        if (error) { res.status(500).json({ message: error.message }) }
        res.status(200).json(results);
    });
}

const getStallImages = (req, res) => {
    conn.query(`SELECT * from stalls_images;`, (error, results) => {
        if (error) { res.status(500).json({ message: error.message }); }
        res.status(200).json(results);
    });
}

const getStallByName = (req, res) => {
    const name = req.params.name;
    conn.query(` SELECT * FROM stalls WHERE city LIKE ? OR NAME LIKE ?`, [name, name], (error, results) => {
        if (error) { res.status(500).json({  message: error.message }) }
        res.status(200).json(results);
    })
}

const getAllAppointmentsForStall = (req, res) => {
    conn.query(`SELECT * FROM stalls INNER JOIN appointments 
        ON stalls.id = appointments.stalls_id;`, (error, results) => {
            if (error) { res.status(500).json({ message: error.message }); }
            res.status(200).json(results);
        })
}
  
const getRandomImageForStallById = (req, res) => {
    const id = req.params.id;
    conn.query(`SELECT * FROM stalls_images WHERE stalls_id = ?
        ORDER BY RAND() LIMIT 1;`, [id], (error, results) => {
            if (error) { res.status(500).json({ message: error.message }); }
            res.status(200).json(results);
        });
}


module.exports = {
    getStalls,
    getStallsById,
    getImagesForStall,
    getStallImages,
    getImagesForStallByName,
    getStallByName,
    getAllAppointmentsForStall,
    getRandomImageForStallById
}