const mysql = require('mysql');

const conn = new mysql.createConnection({
    host: 'localhost',
    user: 'matteo',
    password: 'mysql',
    database: 'dedalo_website',
    port: 3306,
    debug: true
});


// SELECT * FROM products INNER JOIN products_images ON products.id = products_images.products_id GROUP BY products.id

const getProducts = (req, res) => {
    conn.query(`SELECT * from products
    INNER JOIN products_images ON products.id = products_images.products_id GROUP BY products.id`, (error, results) => {
        if (error) { res.status(500).json({ message: error.message }); }
        res.status(200).json(results);
    });
}

const getProductById = (req, res) => {
    const id = req.params.id;
    conn.query(`SELECT * from products 
                INNER JOIN products_images
                ON products.id = products_images.products_id
                WHERE products.id = ?`, [id], (error, results) => {
        if (error) { res.status(500).json({ message: error.message }); }
        res.status(200).json(results);
    });
}

const getImagesForProduct = (req, res) => {
    const id = req.params.id;
    conn.query(` SELECT * FROM products
                INNER JOIN products_images
                ON products.id = products_images.products_id 
                WHERE products.id = ?;`, [id], (error, results) => {
        if (error) { res.status(500).json({ message: error.message }) }
        res.status(200).json(results);
    });
}

module.exports = {
    getProducts,
    getProductById,
    getImagesForProduct
}