const request = require('request');
const strip_tags = require('locutus/php/strings/strip_tags')
const mysql = require('mysql');

const conn = new mysql.createConnection({
    host: 'localhost',
    user: 'matteo',
    password: 'mysql',
    database: 'dedalo_website',
    port: 3306,
    debug: true
});

const url = 'http://www.mercatiniantiquari.com/appuntamenti.html';

const getAppointmentsForStall = (req, res) => {
    const id = req.params.id;
    makeRequest(function(error, data) {
        if (error) { res.status(500).json({ message: 'error - could not get the data' }) }
        res.status(200).json(strip_tags(data));
    });
}

function makeRequest(callback) {
    var request = require('request');
    request.get(url, function(error, res, body) {
		if (!error && res.statusCode === 200) {
            data = body;
            callback(null, data);
        } else {
            callback(error, null);
        }
    });
}

const getAllAppointments = (req, res) => {
    conn.query(`SELECT * FROM appointments 
    INNER JOIN stalls ON appointments.stalls_id = stalls.id
    INNER JOIN stalls_images ON stalls_images.stalls_id = stalls.id GROUP BY appointments.id;`, (error, results) => {
        if (error) { res.status(500).json({ message: error.message }); }
        res.status(200).json(results);
    });
}



const getAppointmentsForStallId = (req, res) => {
    const id = req.params.id;
    conn.query(`SELECT * FROM appointments INNER JOIN stalls 
    ON appointments.stalls_id = stalls.id
    INNER JOIN stalls_images ON stalls_images.stalls_id = stalls.id WHERE stalls.id = ?
    GROUP BY appointments.id;`, [id], (error, results) => {
        if (error) { res.status(500).json({ message: error.message }); }
        res.status(200).json(results);
    });
}



module.exports = {
    getAppointmentsForStall,
    getAllAppointments,
    getAppointmentsForStallId
}