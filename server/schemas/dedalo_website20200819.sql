-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dedalo_website
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dedalo_website` ;

-- -----------------------------------------------------
-- Schema dedalo_website
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dedalo_website` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
USE `dedalo_website` ;

-- -----------------------------------------------------
-- Table `dedalo_website`.`stalls`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`stalls` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`stalls` (
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(100) NULL DEFAULT NULL,
  `street_square` VARCHAR(100) NULL DEFAULT NULL,
  `appointment` VARCHAR(100) NULL DEFAULT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dedalo_website`.`appointments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`appointments` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`appointments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `start_date` VARCHAR(100) NULL DEFAULT NULL,
  `end_date` VARCHAR(100) NULL DEFAULT NULL,
  `stalls_id` INT NOT NULL,
  PRIMARY KEY (`id`, `stalls_id`),
  INDEX `fk_appointments_stalls1_idx` (`stalls_id` ASC) VISIBLE,
  CONSTRAINT `fk_appointments_stalls1`
    FOREIGN KEY (`stalls_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dedalo_website`.`products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`products` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  `image` VARCHAR(150) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dedalo_website`.`images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`images` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NULL DEFAULT NULL,
  `extension` VARCHAR(45) NULL DEFAULT NULL,
  `target` ENUM('product', 'stall') NOT NULL,
  `target_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `products_id` (`target_id` ASC) VISIBLE,
  CONSTRAINT `products_id`
    FOREIGN KEY (`target_id`)
    REFERENCES `dedalo_website`.`products` (`id`),
  CONSTRAINT `stalls_id`
    FOREIGN KEY (`target_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 59
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = '	';


-- -----------------------------------------------------
-- Table `dedalo_website`.`phinxlog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`phinxlog` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`phinxlog` (
  `version` BIGINT NOT NULL,
  `migration_name` VARCHAR(100) NULL DEFAULT NULL,
  `start_time` TIMESTAMP NULL DEFAULT NULL,
  `end_time` TIMESTAMP NULL DEFAULT NULL,
  `breakpoint` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`users` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `salt` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dedalo_website`.`vendors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`vendors` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`vendors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 34
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `dedalo_website`.`vendors_has_stalls`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`vendors_has_stalls` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`vendors_has_stalls` (
  `vendors_id` INT NOT NULL,
  `stalls_id` INT NOT NULL,
  PRIMARY KEY (`vendors_id`, `stalls_id`),
  INDEX `fk_vendors_has_stalls_stalls1_idx` (`stalls_id` ASC) VISIBLE,
  INDEX `fk_vendors_has_stalls_vendors_idx` (`vendors_id` ASC) VISIBLE,
  CONSTRAINT `fk_vendors_has_stalls_stalls1`
    FOREIGN KEY (`stalls_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`),
  CONSTRAINT `fk_vendors_has_stalls_vendors`
    FOREIGN KEY (`vendors_id`)
    REFERENCES `dedalo_website`.`vendors` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
