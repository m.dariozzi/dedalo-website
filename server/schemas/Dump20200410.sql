-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: dedalo_website
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `start_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stalls_id` int NOT NULL,
  PRIMARY KEY (`id`,`stalls_id`),
  KEY `fk_appointments_stalls1_idx` (`stalls_id`),
  CONSTRAINT `fk_appointments_stalls1` FOREIGN KEY (`stalls_id`) REFERENCES `stalls` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (1,'2020-04-05 06:00:00','2020-04-05 18:00:00',17),(2,'2020-05-03 06:00:00','2020-05-03 18:00:00',17),(3,'2020-04-09 08:00:00','2020-04-09 012:00:00',13),(4,'2020-05-09 08:00:00','2020-05-09 18:00:00',13),(5,'2020-06-10 06:00:00','2020-06-10 18:00:00',15);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,'Grande giardiniera in porcellana Capodimonte','CMBEC-0350',NULL,'Una grande giardiniera di capodimonte'),(3,'Antico vaso cinese','CMVAS-0375',NULL,'Un grande vaso cinese molto pregiato'),(4,'Portacarbone in rame','POCAR-0001',NULL,'un portacarbone'),(5,'Portacarbone in rame finemente decorato','POCAR-0002',NULL,'portacarbone decorato'),(6,'Cavalli in ceramica','CMDIV-0001',NULL,'Cavallini in ceramica piccoli'),(7,'Accendino a botte','ACCEN-0001',NULL,'Un bell\'accendino a forma di botte capace di fare una fiammata degno di questo nome');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_images`
--

DROP TABLE IF EXISTS `products_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products_images` (
  `id` int NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_id` int NOT NULL,
  PRIMARY KEY (`id`,`products_id`),
  KEY `fk_images_products1_idx` (`products_id`),
  CONSTRAINT `fk_images_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_images`
--

LOCK TABLES `products_images` WRITE;
/*!40000 ALTER TABLE `products_images` DISABLE KEYS */;
INSERT INTO `products_images` VALUES (1,'assets/images/gallery/CMBEC-0350 – Grande giardiniera in porcellana Capodimonte','.JPG',2),(2,'assets/images/gallery/CMVAS-0375 – Antico vaso cinese','.JPG',3),(3,'assets/images/gallery/POCAR-0001 – Portacarbone in rame','.JPG',4),(4,'assets/images/gallery/POCAR-0002 – Portacarbone in rame decorato finemente','.JPG',5),(5,'assets/images/gallery/CMDIV-0001 – Cavalli in ceramica','.JPG',6),(6,'assets/images/gallery/ACCEN-0001 – Accendino a botte','.JPG',7);
/*!40000 ALTER TABLE `products_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stalls`
--

DROP TABLE IF EXISTS `stalls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stalls` (
  `name` varchar(45) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `street_square` varchar(100) DEFAULT NULL,
  `appointment` varchar(100) DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stalls`
--

LOCK TABLES `stalls` WRITE;
/*!40000 ALTER TABLE `stalls` DISABLE KEYS */;
INSERT INTO `stalls` VALUES ('Mercato di Macerata','Macerata','Sotto le logge a Macerata','Ogni Giovedì della settimana',13,'il mercatino di macerata si tiene sotte le logge a macerata'),('L\'antico e le palme','San Benedetto de Tronto',NULL,NULL,14,'L\'antico e le palme è forse uno dei mercati più importante che c\'è nella Marche'),('Il mercatino di Pescara','Pescara','Bo',NULL,15,'Il mercatino di Pescara sta a Pescara'),('Il mercato di Ascoli Piceno','Ascoli Piceno','Piazza del Popolo','Ogni terza domenica del mese',16,'Questo mercatino viene organizzato ogni terza domenica del mese'),('Pissignano','Campello Sul Clitunno','Lungo la Via Flaminia, all\'altezza delle fonti deL Clitunno','Ogni prima domenica del mese',17,'Il ridente mercatino di Campello sul Clitunno');
/*!40000 ALTER TABLE `stalls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stalls_images`
--

DROP TABLE IF EXISTS `stalls_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stalls_images` (
  `id` int NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stalls_id` int NOT NULL,
  PRIMARY KEY (`id`,`stalls_id`),
  KEY `fk_stalls_images_stalls1_idx` (`stalls_id`),
  CONSTRAINT `fk_stalls_images_stalls1` FOREIGN KEY (`stalls_id`) REFERENCES `stalls` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stalls_images`
--

LOCK TABLES `stalls_images` WRITE;
/*!40000 ALTER TABLE `stalls_images` DISABLE KEYS */;
INSERT INTO `stalls_images` VALUES (1,'assets/images/other/P001','.jpeg',13),(2,'assets/images/other/P002','.jpeg',13),(3,'assets/images/other/P003','.jpeg',17),(4,'assets/images/other/A001','.jpeg',16),(5,'assets/images/other/A002','.jpeg',16),(6,'assets/images/other/A003','.jpeg',16),(7,'assets/images/other/P003','.jpeg',17),(8,'assets/images/other/P003','.jpeg',17),(9,'assets/images/other/P003','.jpeg',17),(10,'assets/images/other/M001','.jpeg',13),(11,'assets/images/other/M002','.jpeg',13),(12,'assets/images/other/M003','.jpeg',13),(13,'assets/images/other/M004','.jpeg',13),(14,'assets/images/other/M005','.jpeg',13),(15,'assets/images/other/M006','.jpeg',13),(16,'assets/images/other/M007','.jpeg',13),(17,'assets/images/other/M008','.jpeg',13),(18,'assets/images/other/M009','.jpeg',13),(19,'assets/images/other/M010','.jpeg',13),(20,'assets/images/other/M011','.jpeg',15);
/*!40000 ALTER TABLE `stalls_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'matteo.dariozzi@hotmail.com','95b37aa06e6c05e23afb472c3a4775b3306bb78adc9507bd388570d667a91ae41132d051af5729bbb0d59e193bbdfa40a8ec9bb6cbc7c5df57af97aac3a1c329','e3db5a8c7c7de6ac83ee10f4c8f32e2d'),(4,'a@a.a','f9156ff428e72bb5657b1682ab9bdb7c54dcfc9709a404d3bf2524b6eb7cb6e0cbc134aa21ef5c4845340fcfd637ffc22b3f5c9e65dac1651d8e654126ce18cb','888fad73fb4ae135e61ea83e52e9896e');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors`
--

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` VALUES (7,'Mazzalupi','Rita'),(8,'Dariozzi','Cautino'),(9,'Scoponi','Nazzareno');
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors_has_stalls`
--

DROP TABLE IF EXISTS `vendors_has_stalls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendors_has_stalls` (
  `vendors_id` int NOT NULL,
  `stalls_id` int NOT NULL,
  PRIMARY KEY (`vendors_id`,`stalls_id`),
  KEY `fk_vendors_has_stalls_stalls1_idx` (`stalls_id`),
  KEY `fk_vendors_has_stalls_vendors_idx` (`vendors_id`),
  CONSTRAINT `fk_vendors_has_stalls_stalls1` FOREIGN KEY (`stalls_id`) REFERENCES `stalls` (`id`),
  CONSTRAINT `fk_vendors_has_stalls_vendors` FOREIGN KEY (`vendors_id`) REFERENCES `vendors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors_has_stalls`
--

LOCK TABLES `vendors_has_stalls` WRITE;
/*!40000 ALTER TABLE `vendors_has_stalls` DISABLE KEYS */;
INSERT INTO `vendors_has_stalls` VALUES (7,13),(7,14),(7,15),(7,16),(8,17),(9,17);
/*!40000 ALTER TABLE `vendors_has_stalls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-10 14:45:31
