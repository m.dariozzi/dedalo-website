-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema dedalo_website
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dedalo_website` ;

-- -----------------------------------------------------
-- Schema dedalo_website
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dedalo_website` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
-- -----------------------------------------------------
-- Schema new_schema1
-- -----------------------------------------------------
USE `dedalo_website` ;

-- -----------------------------------------------------
-- Table `dedalo_website`.`products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`products` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`stalls`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`stalls` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`stalls` (
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`vendors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`vendors` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`vendors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`vendors_has_stalls`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dedalo_website`.`vendors_has_stalls` ;

CREATE TABLE IF NOT EXISTS `dedalo_website`.`vendors_has_stalls` (
  `vendors_id` INT NOT NULL,
  `stalls_id` INT NOT NULL,
  PRIMARY KEY (`vendors_id`, `stalls_id`),
  INDEX `fk_vendors_has_stalls_stalls1_idx` (`stalls_id` ASC) VISIBLE,
  INDEX `fk_vendors_has_stalls_vendors_idx` (`vendors_id` ASC) VISIBLE,
  CONSTRAINT `fk_vendors_has_stalls_vendors`
    FOREIGN KEY (`vendors_id`)
    REFERENCES `dedalo_website`.`vendors` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vendors_has_stalls_stalls1`
    FOREIGN KEY (`stalls_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
