-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dedalo_website
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dedalo_website
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dedalo_website` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
USE `dedalo_website` ;

-- -----------------------------------------------------
-- Table `dedalo_website`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  `image` VARCHAR(150) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`products_images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`products_images` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NULL DEFAULT NULL,
  `extension` VARCHAR(45) NULL DEFAULT NULL,
  `products_id` INT NOT NULL,
  PRIMARY KEY (`id`, `products_id`),
  INDEX `fk_images_products1_idx` (`products_id` ASC) VISIBLE,
  CONSTRAINT `fk_images_products1`
    FOREIGN KEY (`products_id`)
    REFERENCES `dedalo_website`.`products` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
COMMENT = '	';


-- -----------------------------------------------------
-- Table `dedalo_website`.`stalls`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`stalls` (
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(100) NULL DEFAULT NULL,
  `street_square` VARCHAR(100) NULL DEFAULT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`stalls_images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`stalls_images` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NULL DEFAULT NULL,
  `extension` VARCHAR(45) NULL DEFAULT NULL,
  `stalls_id` INT NOT NULL,
  PRIMARY KEY (`id`, `stalls_id`),
  INDEX `fk_stalls_images_stalls1_idx` (`stalls_id` ASC) VISIBLE,
  CONSTRAINT `fk_stalls_images_stalls1`
    FOREIGN KEY (`stalls_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
COMMENT = '	';


-- -----------------------------------------------------
-- Table `dedalo_website`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `salt` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `dedalo_website`.`vendors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`vendors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`vendors_has_stalls`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`vendors_has_stalls` (
  `vendors_id` INT NOT NULL,
  `stalls_id` INT NOT NULL,
  PRIMARY KEY (`vendors_id`, `stalls_id`),
  INDEX `fk_vendors_has_stalls_stalls1_idx` (`stalls_id` ASC) VISIBLE,
  INDEX `fk_vendors_has_stalls_vendors_idx` (`vendors_id` ASC) VISIBLE,
  CONSTRAINT `fk_vendors_has_stalls_stalls1`
    FOREIGN KEY (`stalls_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`),
  CONSTRAINT `fk_vendors_has_stalls_vendors`
    FOREIGN KEY (`vendors_id`)
    REFERENCES `dedalo_website`.`vendors` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dedalo_website`.`appointments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dedalo_website`.`appointments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `start_date` VARCHAR(100) NULL,
  `end_date` VARCHAR(100) NULL,
  `stalls_id` INT NOT NULL,
  PRIMARY KEY (`id`, `stalls_id`),
  INDEX `fk_appointments_stalls1_idx` (`stalls_id` ASC) VISIBLE,
  CONSTRAINT `fk_appointments_stalls1`
    FOREIGN KEY (`stalls_id`)
    REFERENCES `dedalo_website`.`stalls` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
