ALTER TABLE `dedalo_website`.`appointments` 
ADD COLUMN `start_time` TIME NULL AFTER `Title`,
ADD COLUMN `stop_time` TIME NULL AFTER `start_time`;
