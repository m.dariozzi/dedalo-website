-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: dedalo_website
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `stalls_images`
--

LOCK TABLES `stalls_images` WRITE;
/*!40000 ALTER TABLE `stalls_images` DISABLE KEYS */;
INSERT INTO `stalls_images` VALUES (1,'assets/images/other/P001','.jpeg',13),(2,'assets/images/other/P002','.jpeg',13),(3,'assets/images/other/P003','.jpeg',17),(4,'assets/images/other/A001','.jpeg',16),(5,'assets/images/other/A002','.jpeg',16),(6,'assets/images/other/A003','.jpeg',16),(7,'assets/images/other/P003','.jpeg',17),(8,'assets/images/other/P003','.jpeg',17),(9,'assets/images/other/P003','.jpeg',17),(10,'assets/images/other/M001','.jpeg',13),(11,'assets/images/other/M002','.jpeg',13),(12,'assets/images/other/M003','.jpeg',13),(13,'assets/images/other/M004','.jpeg',13),(14,'assets/images/other/M005','.jpeg',13),(15,'assets/images/other/M006','.jpeg',13),(16,'assets/images/other/M007','.jpeg',13),(17,'assets/images/other/M008','.jpeg',13),(18,'assets/images/other/M009','.jpeg',13),(19,'assets/images/other/M010','.jpeg',13),(20,'assets/images/other/M011','.jpeg',13);
/*!40000 ALTER TABLE `stalls_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-02  9:52:27
