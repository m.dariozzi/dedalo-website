require('dotenv').config();
let mysql = require('mysql');
let jwt = require('jsonwebtoken');
let crypto = require('crypto');


const conn = new mysql.createConnection({
    host: 'localhost',
    user: 'matteo',
    password: 'mysql',
    database: 'dedalo_website',
    port: 3306,
    debug: true
});


const login = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    conn.query(`select * from users where email = ?`, [email], (error, results) => {
        if (error) { res.status(500).json({ message: error.message}); }
        if (results.length === 0) {
            res.status(400).json({ message: 'wrong username or password' }); 
        } else {
            let salt = results[0].salt;
            let hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString('hex');

            if (hash === results[0].password) {

                let token = jwt.sign({username: email},
                    process.env.secret, { 
                    expiresIn: '24h'
                });

                return res.status(201).json({ user: results, access_token: token });
            } else {
                return res.status(400).json({ message: 'wrong username or password' });
            }
        }
    });

}

const register = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString('hex');
    
    conn.query(`INSERT INTO users (email, password, salt)
        VALUES (?, ?, ?)`, [email, hash, salt], (error, results) => {
            if (error) { res.status(500).json({ message: error.message }); }
            console.log(results);
            return res.status(200).json({ message: results[0].insertId })
    })
}


module.exports = {
    login,
    register
}

// login(req, res) {
//     let username = req.body.username;
//     let password = req.body.password;

//     //fetch from database

//     if (username && password) {
//         if(username && password) {
//             let token = jwt.sign({username: username},
//             process.env.secret, { 
//                 expiresIn: '24h'
//             });

//             res.json({
//                 success: true,
//                 message: 'authentication successful',
//                 token: token
//             });
//         } else {
//             res.send(403).json({
//                 success: false,
//                 message: 'Incorrect username or password'
//             });
//         } 
//     } else {
//         res.send(400).json({
//             success: false,
//             message: 'Authentication failed, please try again'
//         });
//     }
// // }


// module.exports = {
//     login
// }