require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT || 8080;


const stalls = require('./db/stalls');
const vendors = require('./db/vendors');
const products = require('./db/products');
const appointments = require('./db/appointments');

const auth = require('./auth');

const app = express().use(bodyParser.json());

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});  

/**
 * Vendor routes
 */
app.get('/vendors', vendors.getVendors);

app.get('/vendors/:id', vendors.getVendorsById);

app.get('/vendors/:id/stalls', vendors.getStallsForVendorsByVendorId);


/**
 * Stall routes
 */
app.get('/stalls', stalls.getStalls);

app.get('/stalls/:id', stalls.getStallsById);

app.get('/stalls/:name', stalls.getStallByName);

app.get('/stalls/:id/images', stalls.getImagesForStall);

app.get('/images/:name/stalls', stalls.getImagesForStallByName)

app.get('/stalls/appointments', stalls.getAllAppointmentsForStall);

app.get('/stalls/:id/images/random', stalls.getRandomImageForStallById);


/**
 * Image routes
 */
app.get('/images/stalls', stalls.getStallImages);


/**
 * Products routes
 */
app.get('/products', products.getProducts);

app.get('/products/:id', products.getProductById);

app.get('/products/:id/images', products.getImagesForProduct);


/**
 * Dates / Appointments
 */

app.get('/appointments', appointments.getAllAppointments);

app.get('/appointments/stalls/:id', appointments.getAppointmentsForStallId)


/**
 * Calendar web requests
 */

app.get('/appointments/:id', appointments.getAppointmentsForStall);


/**
 * Auth routes
 */

app.post('/auth/login', auth.login);

app.post('/auth/register', auth.register);


app.listen(port, () => console.log(`listening on port ${port}`));